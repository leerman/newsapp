import { createStackNavigator, createBottomTabNavigator } from 'react-navigation';
import Login from './containers/Login';
import TopNews from './containers/TopNews';
import AllNews from './containers/AllNews';
import FavoriteNews from './containers/FavoriteNews';
import Preview from './components/Preview';

const MainTab = createBottomTabNavigator({
    TopNews: {
        screen: TopNews
    },
    AllNews: {
        screen: AllNews
    },
    FavoriteNews: {
        screen: FavoriteNews
    }
}, {
    tabBarOptions: {
        showLabel: false
    }
});

MainTab.navigationOptions = () => ({
    headerTitle: 'News'
});

export default createStackNavigator({
    Login: {
        screen: Login
    },
    Main: {
        screen: MainTab
    },
    Preview: {
        screen: Preview
    }
}, {
    initialRouteName: 'Login'
});
