import React, { PureComponent } from 'react';
import {
    WebView, ActivityIndicator
} from 'react-native';
import PropTypes from 'prop-types';
import { SafeAreaView } from 'react-navigation';

class Preview extends PureComponent {
    static propTypes={
        navigation: PropTypes.shape({
            getParam: PropTypes.func.isRequired
        }).isRequired
    }

    render() {
        const url = this.props.navigation.getParam('url', '');
        return (
            <SafeAreaView style={{ flex: 1 }}>
                <WebView
                    source={{ url }}
                    startInLoadingState
                    renderLoading={() => <ActivityIndicator />} />
            </SafeAreaView>
        );
    }
}

export default Preview;
