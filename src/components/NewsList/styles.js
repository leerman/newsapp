import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    image: {
        height: 200,
        width: null,
        flex: 1,
        marginHorizontal: -15
    },
    body: {
        paddingBottom: 10
    },
    h3: {
        paddingVertical: 10
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
});
