
import React, { Component } from 'react';
import { Image } from 'react-native';
import PropTypes from 'prop-types';
import {
    Card, CardItem, Thumbnail, Button, Icon, Left, Body, Right, Text, Col, H3
} from 'native-base';
import dateformat from 'dateformat';
import styles from './styles';
import noimage from '../../assets/noimage.png';
import thumbimage from '../../assets/thumbnail.jpg';
import { getId } from '../../helper';

export default class ListCard extends Component {
    static propTypes = {
        item: PropTypes.shape({
            author: PropTypes.string,
            description: PropTypes.string,
            publishedAt: PropTypes.string,
            source: PropTypes.shape({
                id: PropTypes.string,
                name: PropTypes.string
            }).isRequired,
            title: PropTypes.string,
            url: PropTypes.string,
            urlToImage: PropTypes.string
        }).isRequired,
        onSharePress: PropTypes.func.isRequired,
        onCommentPress: PropTypes.func.isRequired,
        onLikePress: PropTypes.func.isRequired,
        removeFavorite: PropTypes.func.isRequired,
        favorites: PropTypes.shape().isRequired,
        onPress: PropTypes.func.isRequired
    }

    onSharePress = () => {
        this.props.onSharePress(this.props.item);
    }

    onCommentPress = () => {
        this.props.onCommentPress(this.props.item);
    }

    onLikePress = () => {
        const {
            onLikePress, item, favorites, removeFavorite
        } = this.props;
        const id = getId(item);
        if (!favorites[id]) {
            onLikePress(item);
        } else {
            removeFavorite(item, favorites);
        }
    }

    onPress=() => {
        const { item, onPress } = this.props;
        if (onPress) {
            onPress(item);
        }
    }

    render() {
        const { item } = this.props;
        const publised = dateformat(new Date(item.publishedAt), 'mmmm dS yyyy "at" h:MM TT');
        const image = !item.urlToImage ? noimage : { url: item.urlToImage };
        const likeIconName = `ios-heart${item.liked ? '' : '-outline'}`;
        const likeIconStyle = item.liked ? { color: 'red' } : {};

        return (
            <Card>
                <CardItem>
                    <Left>
                        <Thumbnail source={thumbimage} />
                        <Body>
                            <Text>
                                { item.source.name }
                            </Text>
                            <Text note>
                                { publised }
                            </Text>
                        </Body>
                    </Left>
                </CardItem>
                <CardItem button onPress={this.onPress}>
                    <Col style={styles.body}>
                        <Image source={image} style={styles.image} />
                        <H3 style={styles.h3}>
                            { item.title }
                        </H3>
                        <Text>
                            { item.description }
                        </Text>
                    </Col>
                </CardItem>
                <CardItem style={{ borderTopColor: '#f3f3f3', borderTopWidth: 1 }}>
                    <Left>
                        <Button transparent onPress={this.onLikePress}>
                            <Icon active name={likeIconName} style={likeIconStyle} />
                            <Text>Like</Text>
                        </Button>
                    </Left>
                    <Body>
                        <Button transparent onPress={this.onCommentPress}>
                            <Icon active name="ios-chatbubbles-outline" />
                            <Text>Comments</Text>
                        </Button>
                    </Body>
                    <Right>
                        <Button transparent onPress={this.onSharePress}>
                            <Icon active name="ios-share-alt-outline" />
                            <Text>Share</Text>
                        </Button>
                    </Right>
                </CardItem>
            </Card>
        );
    }
}
