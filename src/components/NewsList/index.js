import React, { PureComponent } from 'react';
import {
    FlatList, Alert, ActivityIndicator, RefreshControl
} from 'react-native';
import { H1 } from 'native-base';
import PropTypes from 'prop-types';
import { SafeAreaView } from 'react-navigation';
import Card from '../../containers/Card';
import styles from './styles';
import { getId } from '../../helper';

export default class NewsList extends PureComponent {
    static propTypes = {
        error: PropTypes.string,
        getNews: PropTypes.func,
        loading: PropTypes.bool,
        page: PropTypes.number,
        total: PropTypes.number,
        items: PropTypes.arrayOf(
            PropTypes.shape({
                author: PropTypes.string,
                description: PropTypes.string,
                publishedAt: PropTypes.string,
                source: PropTypes.shape({
                    id: PropTypes.string,
                    name: PropTypes.string
                }).isRequired,
                title: PropTypes.string,
                url: PropTypes.string,
                urlToImage: PropTypes.string
            })
        ).isRequired,
        loadFavorites: PropTypes.func.isRequired,
        navigation: PropTypes.shape({
            navigate: PropTypes.func.isRequired
        }).isRequired
    }

    static defaultProps = {
        error: null,
        getNews: null,
        loading: false,
        page: 0,
        total: -1
    }

    componentDidMount() {
        this.loadNews();
        this.props.loadFavorites();
    }

    onSharePress = () => {
        Alert.alert('Message', 'Share is not available');
    }

    onCommentPress = () => {
        Alert.alert('Message', 'Comment is not available');
    }

    onLikePress = () => {
        Alert.alert('Message', 'Like is not available');
    }

    onEndReached=() => {
        this.loadNews();
    }

    onRefresh=() => {
        this.loadNews(0);
    }

    onCardPress=(item) => {
        this.props.navigation.navigate('Preview', {
            url: item.url
        });
    }

    getListFooterComponent = () => {
        if (this.props.loading) {
            return (
                <ActivityIndicator style={{ paddingVertical: 10 }} />
            );
        }
        return null;
    }

    keyExtractor=item => getId(item);

    loadNews=(startPage) => {
        const {
            getNews, page, loading, items, total
        } = this.props;

        const loadPage = (typeof startPage === 'undefined' ? page : startPage) + 1;

        if (getNews && !loading && items.length !== total) {
            getNews(loadPage, typeof startPage !== 'undefined');
        }
    }

    renderItem = ({ item }) => (
        <Card
            item={item}
            onCommentPress={this.onCommentPress}
            onSharePress={this.onSharePress}
            onPress={this.onCardPress} />
    )

    render() {
        const { items, loading } = this.props;

        if (loading && items.length === 0) {
            return (
                <SafeAreaView style={styles.container}>
                    <ActivityIndicator size="large" />
                </SafeAreaView>
            );
        }

        if (items.length === 0) {
            return (
                <SafeAreaView style={styles.container}>
                    <H1>
                        No News
                    </H1>
                </SafeAreaView>
            );
        }

        return (
            <SafeAreaView>
                <FlatList
                    data={items}
                    renderItem={this.renderItem}
                    ListFooterComponent={this.getListFooterComponent}
                    onEndReachedThreshold={5}
                    onEndReached={this.onEndReached}
                    keyExtractor={this.keyExtractor}
                    refreshControl={(
                        <RefreshControl
                            refreshing={loading}
                            onRefresh={this.onRefresh} />
                    )} />
            </SafeAreaView>
        );
    }
}
