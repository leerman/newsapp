import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    bacgroundImage: {
        position: 'absolute'
    },
    safearea: {
        flex: 1
    },
    body: {
        justifyContent: 'center',
        flex: 1
    },
    card: {
        paddingHorizontal: 10,
        paddingVertical: 25,
        backgroundColor: '#ffffff80',
        borderRadius: 5
    },
    inputOutter: {
        borderRadius: 30,
        borderColor: '#737373',
        marginBottom: 20
    },
    signInButton: {
        marginTop: 20
    },
    signInText: {
        color: 'white',
        flex: 1,
        textAlign: 'center'
    }
});
