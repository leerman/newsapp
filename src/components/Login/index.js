import React, { PureComponent } from 'react';
import {
    Text, Image, View
} from 'react-native';
import { Input, Item, Button } from 'native-base';
import PropTypes from 'prop-types';
import { StackActions, NavigationActions, SafeAreaView } from 'react-navigation';
import background from '../../assets/background.jpg';
import styles from './styles';

class Login extends PureComponent {
    static propTypes = {
        auth: PropTypes.func.isRequired,
        error: PropTypes.string
    }

    static defaultProps={
        error: null
    }

    state = {
        login: '',
        pass: ''
    }

    onSignInPress = async () => {
        const { login, pass } = this.state;
        const { auth, navigation } = this.props;

        const success = await auth(login, pass);
        if (success) {
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({ routeName: 'Main' })]
            });
            navigation.dispatch(resetAction);
        }
    }

    onLoginChange = (login) => {
        this.setState({
            login
        });
    }

    onPassChange = (pass) => {
        this.setState({
            pass
        });
    }

    getError=() => {
        const { error } = this.props;
        if (error) {
            return (
                <Text style={{ color: 'red' }}>
                    { error }
                </Text>
            );
        }
        return null;
    }

    render() {
        const { login, pass } = this.state;

        return (
            <SafeAreaView style={styles.safearea}>
                <Image
                    blurRadius={5}
                    source={background}
                    style={styles.bacgroundImage} />
                <View style={styles.body}>
                    <View style={{ paddingHorizontal: 20 }}>
                        <View style={styles.card}>
                            <Item regular style={styles.inputOutter}>
                                <Input
                                    placeholder="Email"
                                    value={login}
                                    onChangeText={this.onLoginChange} />
                            </Item>
                            <Item regular style={styles.inputOutter}>
                                <Input
                                    placeholder="Password"
                                    value={pass}
                                    onChangeText={this.onPassChange} />
                            </Item>
                            {this.getError()}
                            <Button
                                title="Sign In"
                                onPress={this.onSignInPress}
                                style={styles.signInButton}>
                                <Text style={styles.signInText}>
                                    Sign In
                                </Text>
                            </Button>
                        </View>
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

export default Login;
