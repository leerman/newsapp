
import { connect } from 'react-redux';
import Card from '../components/NewsList/Card';
import { add, remove } from '../actions/favariteNews';

const mapDispatchToProps = dispatch => ({
    onLikePress: item => dispatch(add(item)),
    removeFavorite: (item, all) => dispatch(remove(item, all))
});

const mapStateToProps = state => ({
    favorites: state.favoriteNews.items
});

export default connect(mapStateToProps, mapDispatchToProps)(Card);
