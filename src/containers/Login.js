import { connect } from 'react-redux';
import Login from '../components/Login';
import { auth } from '../actions/user';

const mapDispatchToProps = dispatch => ({
    auth: (login, pass) => dispatch(auth(login, pass))
});

const mapStateToProps = state => ({
    loading: state.user.fetching,
    error: state.user.error
});

Login.navigationOptions = {
    header: null
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
