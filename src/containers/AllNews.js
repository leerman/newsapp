import React from 'react';
import { connect } from 'react-redux';
import { Icon } from 'native-base';
import NewsList from '../components/NewsList';
import { getAllNews } from '../actions/allNews';
import { load } from '../actions/favariteNews';
import { getNewsArray } from '../helper';

const mapDispatchToProps = dispatch => ({
    getNews: (page, initial) => dispatch(getAllNews(page, initial)),
    loadFavorites: () => dispatch(load())
});

const mapStateToProps = state => ({
    loading: state.allNews.fetching,
    error: state.allNews.error,
    items: getNewsArray(state.allNews.items, state.favoriteNews.items),
    total: state.allNews.total,
    page: state.allNews.page
});

NewsList.navigationOptions = {
    title: 'Top news',
    tabBarIcon: ({ focused, tintColor }) => <Icon style={{ color: tintColor }} name={`ios-paper${focused ? '' : '-outline'}`} />
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);
