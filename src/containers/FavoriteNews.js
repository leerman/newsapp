import React from 'react';
import { connect } from 'react-redux';
import { Icon } from 'native-base';
import NewsList from '../components/NewsList';
import { load } from '../actions/favariteNews';
import { getNewsArray } from '../helper';

const mapDispatchToProps = dispatch => ({
    loadFavorites: () => dispatch(load())
});

const mapStateToProps = state => ({
    items: getNewsArray(state.favoriteNews.items, state.favoriteNews.items)
});

NewsList.navigationOptions = {
    title: 'Top news',
    tabBarIcon: ({ focused, tintColor }) => <Icon style={{ color: tintColor }} name={`ios-heart${focused ? '' : '-outline'}`} />
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);
