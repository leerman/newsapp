import React from 'react';
import { connect } from 'react-redux';
import { Icon } from 'native-base';
import NewsList from '../components/NewsList';
import { getTopNews } from '../actions/topNews';
import { load } from '../actions/favariteNews';
import { getNewsArray } from '../helper';

const mapDispatchToProps = dispatch => ({
    getNews: (page, initial) => dispatch(getTopNews(page, initial)),
    loadFavorites: () => dispatch(load())
});

const mapStateToProps = state => ({
    loading: state.topNews.fetching,
    error: state.topNews.error,
    items: getNewsArray(state.topNews.items, state.favoriteNews.items),
    total: state.topNews.total,
    page: state.topNews.page
});

NewsList.navigationOptions = {
    title: 'Top news',
    tabBarIcon: ({ focused, tintColor }) => <Icon style={{ color: tintColor }} name={`ios-star${focused ? '' : '-outline'}`} />
};

export default connect(mapStateToProps, mapDispatchToProps)(NewsList);
