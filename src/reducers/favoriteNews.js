import { ADD_FAVORITES, LOAD_FAVORITES } from '../actions/favariteNews';

const initialState = {
    items: {},
    loaded: false
};

export default function (state = initialState, action) {
    switch (action.type) {
        case LOAD_FAVORITES: {
            return {
                ...state,
                items: action.payload.news,
                loaded: true
            };
        }
        case ADD_FAVORITES: {
            return {
                ...state,
                items: {
                    ...state.items,
                    ...action.payload.newNews
                }
            };
        }
        default: {
            return state;
        }
    }
}
