import { REJECT_ALL_NEWS, REQUEST_ALL_NEWS, RESPONSE_ALL_NEWS } from '../actions/allNews';

const initialState = {
    fetching: false,
    error: null,
    items: {},
    total: -1,
    page: 0
};

export default function (state = initialState, action) {
    switch (action.type) {
        case REQUEST_ALL_NEWS: {
            return {
                ...state,
                fetching: true,
                error: null
            };
        }
        case RESPONSE_ALL_NEWS: {
            return {
                ...state,
                fetching: false,
                items: action.payload.isInitial
                    ? action.payload.news
                    : {
                        ...state.items,
                        ...action.payload.news
                    },
                total: action.payload.total,
                page: action.payload.page
            };
        }
        case REJECT_ALL_NEWS: {
            return {
                ...state,
                fetching: false,
                error: action.payload.error
            };
        }
        default: {
            return state;
        }
    }
}
