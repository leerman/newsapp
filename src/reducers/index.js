import { combineReducers } from 'redux';
import user from './user';
import topNews from './topNews';
import allNews from './allNews';
import favoriteNews from './favoriteNews';

export default combineReducers({
    user,
    topNews,
    allNews,
    favoriteNews
});
