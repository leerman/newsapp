import { REJECT_LOGIN, REQUEST_LOGIN, RESPONSE_LOGIN } from '../actions/user';

const initialState = {
    fetching: false,
    error: null
};

export default function (state = initialState, action) {
    switch (action.type) {
        case REQUEST_LOGIN: {
            return {
                ...state,
                fetching: true,
                error: null
            };
        }
        case RESPONSE_LOGIN: {
            return {
                ...state,
                fetching: false
            };
        }
        case REJECT_LOGIN: {
            return {
                ...state,
                fetching: false,
                error: action.payload.error
            };
        }
        default: {
            return state;
        }
    }
}
