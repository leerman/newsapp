import { REJECT_TOP_NEWS, REQUEST_TOP_NEWS, RESPONSE_TOP_NEWS } from '../actions/topNews';

const initialState = {
    fetching: false,
    error: null,
    items: {},
    total: -1,
    page: 0
};

export default function (state = initialState, action) {
    switch (action.type) {
        case REQUEST_TOP_NEWS: {
            return {
                ...state,
                fetching: true,
                error: null
            };
        }
        case RESPONSE_TOP_NEWS: {
            return {
                ...state,
                fetching: false,
                items: action.payload.isInitial
                    ? action.payload.news
                    : {
                        ...state.items,
                        ...action.payload.news
                    },
                total: action.payload.total,
                page: action.payload.page
            };
        }
        case REJECT_TOP_NEWS: {
            return {
                ...state,
                fetching: false,
                error: action.payload.error
            };
        }
        default: {
            return state;
        }
    }
}
