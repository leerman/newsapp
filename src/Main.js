import React from 'react';
import { View, Button } from 'react-native';
import PropTypes from 'prop-types';

const Main = props => (
    <View>
        <Button
            title="Go back"
            onPress={() => {
                props.navigation.goBack();
            }} />
    </View>
);

Main.propTypes = {
    navigation: PropTypes.shape({
        goBack: PropTypes.func.isRequired
    }).isRequired
};

export default Main;
