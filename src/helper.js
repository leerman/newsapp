export const getId = item => item.source.id + item.publishedAt;

export const getNewsArray = (newsObj, favorites) => Object.keys(newsObj).map(key => ({
    ...newsObj[key],
    liked: !!favorites[getId(newsObj[key])]
}));
