import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import promise from 'redux-promise';
import redusers from './reducers';

const store = createStore(
    redusers,
    applyMiddleware(thunkMiddleware, promise)
);

if (module.hot) {
    module.hot.accept(() => {
        // eslint-disable-next-line global-require
        const nextRootReducer = require('./reducers').default;
        store.replaceReducer(nextRootReducer);
    });
}

export default store;
