import { auth as fetchAuth } from '../network';

export const REQUEST_LOGIN = 'REQUEST_LOGIN';
export const RESPONSE_LOGIN = 'RESPONSE_LOGIN';
export const REJECT_LOGIN = 'REJECT_LOGIN';

export const requestLogin = () => ({
    type: REQUEST_LOGIN
});

export const responseLogin = user => ({
    type: RESPONSE_LOGIN,
    user
});

export const rejectLogin = error => ({
    type: REJECT_LOGIN,
    payload: {
        error
    }
});

export const auth = (login, pass) => async (dispatch) => {
    dispatch(requestLogin());
    const { success, error } = await fetchAuth(login, pass);
    if (success) {
        dispatch(responseLogin());
    } else {
        dispatch(rejectLogin(error));
    }

    return success;
};
