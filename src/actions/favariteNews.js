import { AsyncStorage } from 'react-native';
import { getId } from '../helper';

export const LOAD_FAVORITES = 'LOAD_FAVORITES';
export const ADD_FAVORITES = 'ADD_FAVORITES';

export const loadFavorites = news => ({
    type: LOAD_FAVORITES,
    payload: {
        news
    }
});

export const addFavorites = newNews => ({
    type: ADD_FAVORITES,
    payload: {
        newNews
    }
});

export const load = () => async (dispatch, getState) => {
    const state = getState();
    if (!state.favoriteNews.loaded) {
        const keys = await AsyncStorage.getAllKeys();
        const items = await AsyncStorage.multiGet(keys);

        const favorites = items.reduce((prev, next) => {
            const item = JSON.parse(next[1]);
            return {
                ...prev,
                [next[0]]: item
            };
        }, {});

        dispatch(loadFavorites(favorites));
    }
};

export const add = item => async (dispatch) => {
    const id = getId(item);
    await AsyncStorage.setItem(id, JSON.stringify(item));
    dispatch(addFavorites({ [id]: item }));
};

export const remove = (item, all) => async (dispatch) => {
    const id = getId(item);
    await AsyncStorage.removeItem(id);

    delete all[id];
    dispatch(loadFavorites(all));
};
