import { fetchNews } from '../network';

export const REQUEST_ALL_NEWS = 'REQUEST_ALL_NEWS';
export const RESPONSE_ALL_NEWS = 'RESPONSE_ALL_NEWS';
export const REJECT_ALL_NEWS = 'REJECT_ALL_NEWS';

export const requestAllNews = () => ({
    type: REQUEST_ALL_NEWS
});

export const responseAllNews = (news, page, total, isInitial) => ({
    type: RESPONSE_ALL_NEWS,
    payload: {
        news,
        page,
        total,
        isInitial
    }
});

export const rejectAllNews = error => ({
    type: REJECT_ALL_NEWS,
    payload: {
        error
    }
});

export const getAllNews = (page, isInitial) => async (dispatch) => {
    dispatch(requestAllNews());

    const { items, error, total } = await fetchNews('everything', {
        domains: 'bbc.co.uk, techcrunch.com, engadget.com',
        page
    });
    if (!error) {
        const news = items.reduce((prev, next) => ({
            ...prev,
            [next.source.id + next.publishedAt]: next
        }), {});

        dispatch(responseAllNews(news, page, total, isInitial));
    } else {
        dispatch(rejectAllNews(error));
    }
};
