import { fetchNews } from '../network';

export const REQUEST_TOP_NEWS = 'REQUEST_TOP_NEWS';
export const RESPONSE_TOP_NEWS = 'RESPONSE_TOP_NEWS';
export const REJECT_TOP_NEWS = 'REJECT_TOP_NEWS';

export const requestTopNews = () => ({
    type: REQUEST_TOP_NEWS
});

export const responseTopNews = (news, page, total, isInitial) => ({
    type: RESPONSE_TOP_NEWS,
    payload: {
        news,
        page,
        total,
        isInitial
    }
});

export const rejectTopNews = error => ({
    type: REJECT_TOP_NEWS,
    payload: {
        error
    }
});

export const getTopNews = (page, isInitial) => async (dispatch) => {
    dispatch(requestTopNews());

    const { items, error, total } = await fetchNews('topHeadlines', {
        country: 'us',
        page
    });
    if (!error) {
        const news = items.reduce((prev, next) => ({
            ...prev,
            [next.source.id + next.publishedAt]: next
        }), {});

        dispatch(responseTopNews(news, page, total, isInitial));
    } else {
        dispatch(rejectTopNews(error));
    }
};
