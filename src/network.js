import NewsApi from 'newsapi';

let newsapi = null;
// let newsapi = new NewsApi('b297ad46dd814872a7a4feec8457ef16');

export const auth = async (email, password) => {
    const result = await fetch('https://adyaxmobile.eu.auth0.com/dbconnections/signup', {
        method: 'POST',
        body: JSON.stringify({
            email,
            password,
            connection: 'News',
            client_id: 'icyhpvdBV9YQrT32u3SLpJ3jpGvxQ2c0'
        })
    });

    // newsapi = new NewsApi('b297ad46dd814872a7a4feec8457ef16');
    // return {
    //     success: true
    // };

    if (result.status === 200) {
        newsapi = new NewsApi('b297ad46dd814872a7a4feec8457ef16');
        return {
            success: true
        };
    }

    if (result.status === 400) {
        const error = await result.json();
        return {
            success: false,
            ...error
        };
    }

    return {
        success: false,
        error: 'Unknown error'
    };
};

export const fetchNews = async (mode, options = {}) => {
    const result = await newsapi.v2[mode]({
        pageSize: 10,
        ...options
    });

    if (result.status === 'ok') {
        return {
            items: result.articles,
            total: result.totalResults
        };
    }

    return {
        error: result.message
    };
};
